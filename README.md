# docker
docker run --init -d --name=home-assistant -v $(pwd):/config -v /etc/localtime:/etc/localtime:ro --net=host homeassistant/home-assistant
docker run --init -d --name=home-assistant -v $(pwd):/config -v /etc/localtime:/etc/localtime:ro --net=host homeassistant/armhf-homeassistant

## check config
docker run -ti -v $(pwd):/config --rm homeassistant/home-assistant python3 -m homeassistant -c /config --script check_config
