{
    "data": {
        "config": {
            "resources": [
                {
                    "type": "module",
                    "url": "/local/config-template-card.js"
                },
                {
                    "type": "js",
                    "url": "/local/card-tools.js?track=true"
                },
                {
                    "type": "js",
                    "url": "/local/useful-markdown-card.js"
                }
            ],
            "title": "All",
            "views": [
                {
                    "badges": [
                        "sensor.wien_hohew_sun_last_hour",
                        "sun.sun",
                        "sensor.time",
                        "sensor.season",
                        "sensor.nilan_air_in_out_delta",
                        "sensor.heatpump_active"
                    ],
                    "cards": [
                        {
                            "entity": "weather.home",
                            "type": "weather-forecast"
                        },
                        {
                            "entity": "weather.wien",
                            "name": "ZAMG",
                            "type": "weather-forecast"
                        },
                        {
                            "entities": [
                                "input_number.vent_speed",
                                "input_number.air_temp_setpoint"
                            ],
                            "show_header_toggle": false,
                            "title": "Nilan Setpoints",
                            "type": "entities"
                        },
                        {
                            "columns": 3,
                            "entities": [
                                {
                                    "entity": "sensor.outdoor_temp"
                                },
                                {
                                    "entity": "sensor.inlet_temp_after_heater"
                                },
                                {
                                    "entity": "sensor.user_panel"
                                },
                                {
                                    "entity": "sensor.air_room_temp"
                                },
                                {
                                    "entity": "sensor.outlet_temp"
                                },
                                {
                                    "entity": "sensor.humidity"
                                }
                            ],
                            "title": "Air",
                            "type": "glance"
                        },
                        {
                            "content": "## Zamg vienna night \n[[ sensor.zamg_weather_night.state ]]\n",
                            "type": "custom:useful-markdown-card"
                        },
                        {
                            "entities": [
                                "sensor.outdoor_temp",
                                "sensor.user_panel",
                                "sensor.inlet_temp_after_heater",
                                "sensor.humidity",
                                "sensor.air_room_temp"
                            ],
                            "refresh_interval": 300,
                            "type": "history-graph"
                        },
                        {
                            "hold_action": {
                                "action": "none"
                            },
                            "image": "https://www.zamg.ac.at/zamgWeb/pict/tawes/tawes_plone_tl_11040_6h.gif",
                            "tap_action": {
                                "action": "none"
                            },
                            "type": "picture"
                        },
                        {
                            "hold_action": {
                                "action": "none"
                            },
                            "image": "https://www.zamg.ac.at/zamgWeb/pict/tawes/tawes_plone_rr_11040_6h.gif",
                            "tap_action": {
                                "action": "none"
                            },
                            "type": "picture"
                        }
                    ],
                    "path": "default_view",
                    "title": "Climate"
                },
                {
                    "badges": [
                        "sensor.pv_prod",
                        "sensor.pv_prod_avrg",
                        "sensor.building_consumption",
                        "sensor.pv_self_consumed",
                        "sensor.pv_grid_load",
                        "sensor.pv_buying_from_grid",
                        "sensor.pv_surplus_selling",
                        "sensor.pv_surplus_selling_avrg",
                        "sensor.voltronic_pv_battery_charge",
                        "sensor.voltronic_pv_battery_voltage"
                    ],
                    "cards": [
                        {
                            "cards": [
                                {
                                    "entity": "sensor.pv_buying_from_grid",
                                    "max": 4000,
                                    "min": 0,
                                    "name": "Buying",
                                    "severity": {
                                        "green": 0,
                                        "red": 0,
                                        "yellow": 0
                                    },
                                    "type": "gauge",
                                    "unit": "W"
                                },
                                {
                                    "entity": "sensor.pv_consumption_coverage_percent",
                                    "max": 100,
                                    "min": 0,
                                    "name": "PV coverage",
                                    "severity": {
                                        "green": 1,
                                        "red": 0,
                                        "yellow": 0.9
                                    },
                                    "theme": "default",
                                    "type": "gauge",
                                    "unit": "%"
                                },
                                {
                                    "entity": "sensor.pv_surplus_selling",
                                    "max": 4000,
                                    "min": 0,
                                    "name": "Selling",
                                    "severity": {
                                        "green": 0.1,
                                        "red": 0,
                                        "yellow": 0
                                    },
                                    "type": "gauge",
                                    "unit": "W"
                                }
                            ],
                            "type": "horizontal-stack"
                        },
                        {
                            "entities": [
                                "sensor.production",
                                "sensor.buying_from_grid",
                                "sensor.self_consumed",
                                "sensor.surplus_selling"
                            ],
                            "refresh_interval": 300,
                            "type": "history-graph"
                        },
                        {
                            "columns": 3,
                            "entities": [
                                {
                                    "entity": "sensor.pv_prod_daily"
                                },
                                {
                                    "entity": "sensor.pv_self_consumed_daily"
                                },
                                {
                                    "entity": "sensor.pv_buying_grid_daily"
                                },
                                {
                                    "entity": "sensor.pv_grid_load_daily"
                                },
                                {
                                    "entity": "sensor.pv_surplus_selling_daily"
                                }
                            ],
                            "show_icon": true,
                            "show_name": true,
                            "show_state": true,
                            "title": "Today",
                            "type": "glance"
                        },
                        {
                            "hold_action": {
                                "action": "none"
                            },
                            "image": "https://www.zamg.ac.at/zamgWeb/pict/tawes/tawes_plone_so_11040_6h.gif",
                            "tap_action": {
                                "action": "navigate",
                                "navigation_path": "https://www.zamg.ac.at/zamgWeb/common/display_js_window.php?imgPath=/zamgWeb/pict/tawes/tawes_plone_so_11040_1h.gif&imgTitle=TAWES-Verlaufsgraphik&imgSource=%26copy%3B+ZAMG&imgWidth=1152&imgHeight=360"
                            },
                            "type": "picture"
                        },
                        {
                            "columns": 3,
                            "entities": [
                                {
                                    "entity": "sensor.pv_buying_grid_yesterday"
                                },
                                {
                                    "entity": "sensor.pv_grid_load_yesterday"
                                },
                                {
                                    "entity": "sensor.pv_prod_yesterday"
                                },
                                {
                                    "entity": "sensor.pv_self_consumed_yesterday"
                                },
                                {
                                    "entity": "sensor.pv_surplus_selling_yesterday"
                                }
                            ],
                            "title": "Yesterday",
                            "type": "glance"
                        }
                    ],
                    "title": "PV"
                },
                {
                    "badges": [],
                    "cards": [
                        {
                            "entities": [
                                {
                                    "entity": "input_number.water_top_setpoint"
                                },
                                {
                                    "entity": "input_number.water_bottom_setpoint"
                                }
                            ],
                            "show_header_toggle": false,
                            "title": "Nilan Setpoints",
                            "type": "entities"
                        },
                        {
                            "entities": [
                                "sensor.control_state_human_readable",
                                "sensor.heatpump_active"
                            ],
                            "type": "history-graph"
                        },
                        {
                            "columns": 4,
                            "entities": [
                                {
                                    "entity": "sensor.hot_water_top"
                                },
                                {
                                    "entity": "sensor.hot_water_bottom"
                                },
                                {
                                    "entity": "sensor.heatpump_active"
                                },
                                {
                                    "entity": "sensor.control_state_human_readable"
                                },
                                {
                                    "entity": "sensor.nilan_water_heating"
                                },
                                {
                                    "entity": "sensor.hot_water_bottom_stats"
                                },
                                {
                                    "entity": "sensor.water_heating_today"
                                },
                                {
                                    "entity": "sensor.electrical_heater_active"
                                }
                            ],
                            "title": "Water",
                            "type": "glance"
                        },
                        {
                            "entities": [
                                "sensor.hot_water_top",
                                "sensor.hot_water_bottom"
                            ],
                            "refresh_interval": 300,
                            "type": "history-graph"
                        },
                        {
                            "entities": [
                                {
                                    "entity": "automation.boiler_nighttime_reduction"
                                },
                                {
                                    "entity": "automation.boiler_restore_dailight"
                                },
                                {
                                    "entity": "input_number.water_daytime_backup"
                                }
                            ],
                            "title": "Nighttime reduction",
                            "type": "entities"
                        }
                    ],
                    "title": "Hot Water"
                },
                {
                    "badges": [],
                    "cards": [
                        {
                            "entity": "media_player.kodi",
                            "type": "media-control"
                        },
                        {
                            "entity": "media_player.picoreplayer1",
                            "type": "media-control"
                        }
                    ],
                    "title": "Media"
                }
            ]
        }
    },
    "key": "lovelace",
    "version": 1
}